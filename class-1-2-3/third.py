# write and read file
dummy_file = open('./dummy-file.txt', 'w+')
dummy_file.write('hello world')
dummy_file.seek(0)

print(f'FILE:\n{dummy_file.read()}')
dummy_file.close()

# append inputted content in file
content = input('What do you want to add to dummy-file.txt?\n')
dummy_file = open('./dummy-file.txt', 'a+')
dummy_file.write(f'\n{content}')
dummy_file.seek(0)

print(f'FILE:\n{dummy_file.read()}')
dummy_file.close()

# attempt opening non-existing file
try:
	f = open('./test.txt', 'r')
	print(f.read())
except IOError:
	print(f'\nERROR: I/O error')
except FileNotFoundError:
	print(f'\nERROR: file not found')