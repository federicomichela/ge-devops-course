# LESSON 1 ASSIGNMENT
# @author Michela Federico <federico.michela@gmail.com>
# Disclaimer: different techniques of formatting strings have been used for demonstration purposes.

import sys
import datetime

def exercise_a():
	print("\nA)")
	print("1. Create a variable name first with value 7.")
	print("2. Create a variable name second with value 44.3.")
	print("3. Print the result of adding first to second.")
	print("4. Print the result of multiplying first by second.")
	print("5. Print the result of dividing second by first.")

	print("\nANSWER:")
	first = 7
	second = 44.33
	print('{x} + {y} = {z}'.format(x=first, y=second, z=first + second))
	print('{x} * {y} = {z}'.format(x=first, y=second, z=first * second))
	print('{y} / {x} = {z}'.format(x=first, y=second, z=first / second))

def exercise_b():
	print("\nB)")
	print("What will be the values of a, b, c at the end?")
	print("a = 8")
	print("a = 17")
	print("a = 9")
	print("b = 6")
	print("c = a+b")
	print("b = c+a")
	print("b = 8")

	print("\nANSWER:")
	# a = 8 > [overridden] 17 > [overridden] 9 [final]
	# b = 6 > [overridden] (15 + 9) 24 > [overridden] 8 [final]
	# c = (9 + 6) 15 [final]
	a = 8
	a = 17
	a = 9
	b = 6
	c = a + b
	b = c + a
	b = 8
	print('%i %i %i' % (a, b, c))

def exercise_c():
	print("\nC)")
	print("Is there a difference between the two lines below? Why?")
	print("name = “john”")
	print("name= ‘john’")

	print("\nANSWER")
	print("They're both the same and acceptable.")
	print("However python prefers double quotes, so single quotes can be used inside (instead of escaping them)")
	print("which makes the code more readable")
	print("eg. 'Building Microfronteds' by Luca Mezzalira is the book I just finished to re")
	print('eg. \'Building Microfronteds\' by Luca Mezzalira is the book I just finished to re')

	print("\nWhat is the issue with the code below?")
	print("my_number = 5+5")
	print("print(\"result is: \"+my_number)")

	print("\nANSWER: we're trying to concatenate a string with an int")

	print("\nSuggest an edit.")
	print("\nANSWER:")
	my_number = 5 + 5
	print("(casting to string) result is: " + str(my_number))
	print("(casting to int) result is: %i" % my_number)

def exercise_d():
	print("\nD)")
	print("What will be the output?\n")
	print("x = 5")
	print("y = 2.36")
	print("print (x+int (y) )")

	my_guess = 7
	x = 5
	y = 2.36
	result = x + int(y)
	print("\nANSWER:")
	print("{x} + {y} = {z} ({expected})".format(x=x, y=y, z=result, expected=("right guess" if my_guess == result else "wrong guess")))

def exercise_e():
	print("\nE)")
	print("1. Create two variables named X and Y.")
	print("2. Print “BIG” if X is bigger than Y.")
	print("3. Print “small” if X is smaller than Y.")

	print("\nANSWER:")
	x = input("give a number: ")
	y = input("give another number: ")
	print("{result}".format(result="BIG" if x > y else "small" if x < y else "equal"))
	print(f"The bigger number is {max(x, y)}")

def exercise_f():
	print("\nF)")
	print("1. Create a variable and initialize it with a number 1-4.")
	print("2. Create 4 conditions (if-elif) which will check the variable.")
	print("3. Print the season name accordingly:\n")

	print("- 1 = summer")
	print("- 2 = winter")
	print("- 3 = fall")
	print("- 4 = spring")

	print("\nANSWER:")
	seasons = ("summer", "winter", "fall", "spring")
	season = None
	while season not in range(1, 5):
		season = int(input("Enter a number between 1 and 4 (extremes included): "))
	print("You've chosen '{season}' season".format(season=seasons[season-1]))

def exercise_challenge():
	print("\nCHALLENGE)")
	print("Fix the following code without changing a or b:\n")

	print("a = 8")
	print("b = \"123\"")
	print("print(a+b)")

	print("\nANSWER:")
	a = 8
	b = "123"
	print("Concatenating {a} + {b} = {result}".format(a=a, b=b, result=str(a) + b))
	print("Summing {a} + {b} = {result}".format(a=a, b=b, result=a + int(b)))

def reverse_string(string):
	return string[::-1]

def start(exercise_list):
	print("### Welcome to the DevOps course from Global Expert ###")
	print("This is Michela's implementation of the assigment to the first class.")

	print(f"\nPython version {sys.version.split(" ")[0]}")
	print(datetime.datetime.now())

	your_name = input("\nBefore we start, let's get to know each other.\nWhat's your name?\n")
	print(f"Hi {your_name}, I hope you'll enjoy this session!")

	print(f"\nFUN FACTS about your name:")
	print(f"Did you know that your name reversed is {reverse_string(your_name)}?!")
	print(f"And that it's made of {len(your_name)} characters?")

	exit = input("\nAlright then! Are you ready to dig in? ")

	if exit.lower() in ["n", "no", "nope"]:
		raise SystemExit("Oh ok, bye bye then!")

	print("Awesome! Let's dive in :D\n")
	quit = False

	while not quit:
		print("\n**********************************\nPick an exercise from the list below:")
		for index, exercise in enumerate(exercise_list, start=1):
			print(f"{index}. {exercise}")

		choice_of_exercise = input(f"Which exercise would you like to check out? [1-{len(exercise_list)} or 'q' to quit] ")

		if (choice_of_exercise == 'q'):
			number_check = "yes! [MATCH]" if 120 > 5 and 120 < 200 else "no"
			print(f"\nBefore you go, is it true that 120 is bigger than 5 and smaller than 200? {number_check}")
			raise SystemExit("\nOk then, bye bye")

		else:
			run_exercise(exercise_list[int(choice_of_exercise) - 1])

def get_exercise_list(scope):
	exercise_list = [item for item in scope if item.startswith("exercise_")]
	return exercise_list

def run_exercise(exercise_name):
    this_module = __import__(__name__)  # Get reference to the current module
    function = getattr(this_module, exercise_name)

    if callable(function):  # Correct indentation for if block
        function()
    else:                   # Correct indentation for else block
        print(f"Error: '{exercise_name}' is not available. Check another exercise!")

start(get_exercise_list(dir()))