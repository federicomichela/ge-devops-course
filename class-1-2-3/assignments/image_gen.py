from PIL import Image, ImageDraw, ImageFont
import os

assets_dir = os.path.join(os.getcwd(), "assignments/assets")
os.makedirs(assets_dir, exist_ok=True)


def create_image(text="Hello, Pillow!"):
    text = text or "Hello, Pillow!"
    print(f"Creating image with text '{text}'")

    # Create a new image with a white background
    width, height = 400, 200
    image = Image.new('RGB', (width, height), 'white')

    # Initialize the drawing context
    draw = ImageDraw.Draw(image)

    # Draw a rectangle
    draw.rounded_rectangle(((50, 50), (350, 150)), radius=50, outline='#404E4D', fill='#C3423F', width=10)

    # Load a font
    try:
        font_path = os.path.join(assets_dir, "Railway.otf")
        font = ImageFont.truetype(font_path, 40)
    except IOError:
        print("Font not found")
        font = ImageFont.load_default()

    # Calculate text size and position
    text_bbox = draw.textbbox((0, 0), text, font=font)
    text_width = text_bbox[2] - text_bbox[0]
    text_height = text_bbox[3] - text_bbox[1]

    # Correct vertical alignment
    ascent, descent = font.getmetrics()
    total_height = ascent + descent
    text_y_offset = (total_height - text_height) // 2

    # Center text
    text_x = (width - text_width) // 2
    text_y = (height - text_height) // 2 - text_y_offset

    # Draw the text
    draw.text((text_x, text_y), text, fill='white', font=font)

    # Save the image
    image_path = os.path.join(assets_dir, "custom-image.png")
    image.save(image_path)

    return image_path
