import os
import base64
from flask import Flask, render_template, render_template_string
from image_gen import create_image

print("*** Second Assignment ***")

try:
    a = 1 / 0
except ZeroDivisionError:
    print("\n1/2. Division by zero")

try:
    x = 1
finally:
    print("\n3/4/5.finally - could be caught with syntax error but formatting evaluation runs at"
          " parsing time so adding it wouldn't work anyway")

print("\n6. IOError captures issues with stdin/out like managing with files\n"
      "ZeroDivisionError is self explanatory - operation which diver is evaluated to zero will throw an error")

# Ensure the 'assets' directory exists
assignments_dir = os.path.join(os.getcwd(), "assignments")  # Use os.path.join for better cross-platform compatibility
templates_dir = os.path.join(assignments_dir, "templates")  # Use os.path.join for better cross-platform compatibility
assets_dir = os.path.join(assignments_dir, "assets")        # Use os.path.join for better cross-platform compatibility
os.makedirs(assets_dir, exist_ok=True)                      # Create if it doesn't exist

try:
    file_path = os.path.join(assets_dir, "words.txt")
    file1 = open(file_path, "w+")
    file1.write("Michela")
    file1.seek(0)
    print(f"\n7/8/9\n{file1.read()}")
    file1.close()
except FileNotFoundError:
    print("FileNotFoundError")
except FileExistsError:
    print("FileExistsError")

try:
    file_path = os.path.join(assets_dir, "hebrew_text.txt")
    with open(file_path, "w", encoding="utf-8") as file:
        file.write("10. Hello world 🦋 שלום עולם 🦋 ہیلو ورلڈ ")
except FileNotFoundError:
    print("FileNotFoundError")
except FileExistsError:
    print("FileExistsError")

app = Flask(__name__, template_folder=templates_dir)

@app.route("/")
@app.route("/<user_name>")
def welcome(user_name=''):
    return render_template("home.html", user_name=user_name)

@app.route("/content")
@app.route("/content/<file_name>")
def content(file_name=None):
    file = None
    if not file_name:
        return "No file provided", 200

    try:
        file_path = os.path.join(assets_dir, file_name)
        file = open(file_path, "r", encoding="utf-8")
        file_content = file.read()
        return file_content, 200
    except FileNotFoundError:
        return "File not found", 404
    except:
        return "Something went wrong", 500
    finally:
        file and file.close()

@app.route("/register")
def register():
    file = None

    try:
        file_path = os.path.join(assets_dir, "db.txt")
        file = open(file_path, "a")
        file.write("hello")
        return "success", 201
    except Exception as e:
        print(f"Something went wrong: {e}")
        return "Something went wrong", 500
    finally:
        file.close()

@app.route("/image-gen")
@app.route("/image-gen/<custom_text>")
def img_gen(custom_text=None):
    img_file = None
    img_path = create_image(custom_text)
    try:
        img_file = open(img_path, "rb")
        img_base64 = base64.b64encode(img_file.read()).decode("utf-8")
        html = f'''
        <html>
        <body>
            <h1>Generated Image</h1>
            <img src="data:image/png;base64,{img_base64}" alt="Generated Image">
        </body>
        </html>
        '''
        return render_template_string(html)
    except FileNotFoundError:
        return "File not found", 404
    except:
        return "Something went wrong", 500
    finally:
        img_file.close()

app.run(host="127.0.0.1", port=30000)