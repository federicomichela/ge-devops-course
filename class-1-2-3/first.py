age = 34
name = 'Michela'
show_age = True
num_list = [10, 20, 30]
person_me = { 'name': name, 'age': age }

print("Sum of %s is %i" % (str(num_list), sum(num_list)))

if show_age:
	print('{name} is {age}'.format(name=person_me['name'], age=person_me['age']))

	if person_me['age'] > 30 and person_me['age'] <= 35:
		print('She\'s not so old...yet!')
else:
	print('Hello world, this is %s' % name)

print('\n\nNow, let\'s get to know each other')

# NOTE: use raw_input if using python < 3
your_name = input('What\'s your name?')
print('Hello %s, nice meeting you!' % your_name)


